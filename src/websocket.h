#pragma once
#include <ESPAsyncWebServer.h>
#include <map>
#include <ArduinoJson.h>

class WSClientList;
class Val;

class WSClient {
    AsyncWebSocketClient * client;
    WSClientList * list;
    public:
    void buf_data(AwsFrameInfo * info, uint8_t *data, size_t len);
    WSClient(AsyncWebSocketClient * client, WSClientList * list);
    ~WSClient();
};

class WSClientList {
    WSClientList();    

    public:
    static WSClientList & get();

    std::map<std::string, std::unique_ptr<Val>> vals;
    std::map<uint32_t, WSClient> clients;
    
    void bind(AsyncWebSocket &ws);
    void wsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len);

    void add_client(AsyncWebSocketClient * client);
    void remove_client(AsyncWebSocketClient * client);
    std::iterator<std::forward_iterator_tag, AsyncWebSocketClient*> all_clients();
    void buf_data(AsyncWebSocketClient * client, AwsFrameInfo * info, uint8_t *data, size_t len);
    std::function<void(JsonDocument&)> on_data;
    std::function<void(AsyncWebSocketClient * client)> on_connect;

    void set_val(std::string key, double value);
    void set_val(std::string key, std::string value);
};


class Val {
    public:
    virtual void publish(const JsonObject & obj, const std::string key) { };
};

class NumberVal: public Val {
    double number;
    public:
    void publish(const JsonObject & obj, const std::string key) override {
        obj[(char*)key.c_str()] = number;
    }
    NumberVal(double number) : number(number) {};
};

class StringVal: public Val {
    std::string string;
    public:
    void publish(const JsonObject & obj, const std::string key) override {
        obj[(char*)key.c_str()] = (char*)string.c_str();
    }
    StringVal(std::string string) : string(string) {};
};