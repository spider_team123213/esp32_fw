#pragma once
#include <array>

struct Coords3d {
    double x;
    double y;
    double z;

    constexpr Coords3d operator-(const Coords3d b)  {
        return {x - b.x, y - b.y, z - b.z};
    }
    constexpr Coords3d operator+(const Coords3d b)  {
        return {x + b.x, y + b.y, z + b.z};
    }
    void operator+=(const Coords3d b)  {
        x += b.x;
        y += b.y;
        z += b.z;
    }
    constexpr Coords3d operator*(const Coords3d & b)  {
        return {x * b.x, y * b.y, z * b.z};
    }
    constexpr Coords3d operator/(const double b) {
        return {x / b, y / b, z / b};
    }
    constexpr Coords3d operator*(const double b) {
        return {x * b, y * b, z * b};
    }

    constexpr double sum() {
        return x + y + z;
    }

    constexpr Coords3d linearMultiply(const std::vector<Coords3d> & matrix) {
        return {
            operator*(matrix[0]).sum(), operator*(matrix[1]).sum(), operator*(matrix[2]).sum()
        };
    }

    constexpr Coords3d rot_y_90() {
        return {y, -x, z};
    }

    constexpr Coords3d rot_y_180() {
        return {-x, -y, z};
    }

    constexpr Coords3d rot_y_270() {
        return {-x, y, z};
    }
};

struct Coords2d {
    double x;
    double y;
    constexpr Coords2d operator-(const Coords2d b) {
        return {x - b.x, y - b.y};   
    };
    constexpr Coords2d operator+(const Coords2d b) {
        return {x + b.x, y + b.y};
    }
};

constexpr Coords2d operator*(const double a, const Coords2d b) {
    return {a * b.x, a * b.y};
}