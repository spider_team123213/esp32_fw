#pragma once
#include "SpiderStructure.h"
extern ServoBoard servoBoard1;
extern ServoBoard servoBoard2;
auto spider = Spider();

void init_spider() {
    for (auto i = 0; i < 4; i++) {
        auto k = i * 3;
        if (i % 2) {
            spider.legs[i].joints[0].init(&servoBoard1, k + 1, 90);
            spider.legs[i].joints[1].init(&servoBoard1, k + 2, 45);
            spider.legs[i].joints[2].init(&servoBoard1, k, 135);
        }
        else {
            spider.legs[i].joints[0].init(&servoBoard1, k, 90);
            spider.legs[i].joints[1].init(&servoBoard1, k + 1, 45);
            spider.legs[i].joints[2].init(&servoBoard1, k + 2, 135);
        }
    }

    for (auto i = 4; i < 8; i++) {
        auto k = (i - 4) * 3;
        if (i % 2) {
            spider.legs[i].joints[0].init(&servoBoard2, k + 1, 90);
            spider.legs[i].joints[1].init(&servoBoard2, k + 2, 45);
            spider.legs[i].joints[2].init(&servoBoard2, k, 135);
        }
        else {
            spider.legs[i].joints[0].init(&servoBoard2, k, 90);
            spider.legs[i].joints[1].init(&servoBoard2, k + 1, 45);
            spider.legs[i].joints[2].init(&servoBoard2, k + 2, 135);
        }
    }

    spider.setCoords({225, 0, -350});

    servoBoard2.full_update();

    delay(1000);
    servoBoard1.full_update();
}