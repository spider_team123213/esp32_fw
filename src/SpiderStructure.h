#pragma once
#include "ServoBoard.h"
#include <Arduino.h>
#include <tuple>
#include "geometry.h"

static constexpr double sina(double angle) {
  return sin(angle * PI / 180);
}

static constexpr double asina(double s) {
  return asin(s) * 180.0 / PI;
}

static constexpr double cosa(double angle) {
  return cos(angle * PI / 180);
}

static constexpr double atan2a(double y, double x) {
  return atan2(y, x) * 180.0 / PI;
}

static constexpr double point_distance(Coords2d cs1, Coords2d cs2) {
  return sqrt(pow(cs1.x - cs2.x, 2) + pow(cs1.y - cs2.y, 2));
}

static uint8_t circle_intersection(Coords2d c1, double r1, Coords2d c2, double r2, Coords2d out[2]) {
  //https://litunovskiy.com/gamedev/intersection_of_two_circles/
  double d = point_distance(c1, c2);
  if (d > r1 + r2) return 0;
  double b = (pow(r2,2) - pow(r1, 2) + pow(c1.x - c2.x, 2) + pow(c1.y - c2.y, 2))
    / (2 * d);
  double a = d - b;
  double h = sqrt(r2 * r2 - b*b);

  Coords2d P0 = c1 + (a/d) * (c2 - c1);

  out[0] = {
    P0.x + (c2.y - c1.y) / d * h, 
    P0.y - (c2.x - c1.x) / d * h
  };

  out[1] = {
    P0.x - (c2.y - c1.y) / d * h, 
    P0.y + (c2.x - c1.x) / d * h
  };

  return 2;
}



class Joint {
public:
    ServoBoard * brd;
    uint8_t servo;

    void move(uint8_t pos) {
        brd->pos[servo] = pos;
    }
    void init(ServoBoard * brd_, uint8_t servo_, uint8_t pos) {
        brd = brd_;
        servo = servo_;
        brd->pos[servo] = pos;
    }
    inline uint16_t angle() {
      return brd->pos[servo];
    }
    //operator uint16_t() const { return brd->pos[servo]; }
    double dbl() const { return brd->pos[servo]; }
};

class Leg {
  static constexpr double bone0_len = 60;
  static constexpr double bone1_len = 245;
  static constexpr double bone2_len = 200;
public:
  Joint joints[3];
  using angles = std::vector<double>;

  void moveTo(angles as) {
    if (as.size() == 3) {
      joints[0].move(90 + as[0]);
      joints[1].move(90 + as[1]);
      joints[2].move(180 + as[2]);
    }
  }

#define t(...) // Serial.printf(__VA_ARGS__)
  static angles calcAngles(Coords3d cs) {
    t("cs: %f %f %f ", cs.x, cs.y, cs.z); 

    // Находим угол первого сустава
    auto j0 = atan2a(cs.y, cs.x);
    t("j0: %f ", j0);

    // Проецируем координаты в два измерения с точкой отсчета во втором суставе.
    Coords2d t2d = { cs.x / cosa(j0) - bone0_len, cs.z }; 
    t("t2d: %f %f ", t2d.x, t2d.y);


    // Находим точку для третьего сустава.
    Coords2d data[2];
    if (circle_intersection({0, 0}, bone1_len, {t2d.x, t2d.y}, bone2_len, data) != 2) {
      // Не найдено подходящих - целевая точка вне диапазона.
      Serial.printf("calcAngles: fail %f %f %f\n", cs.x, cs.y, cs.z);
      return {};
    }

    // Пока берем только верхнюю точку
    auto j2_cs = data[1];
    t("j2_cs: %f %f ", j2_cs.x, j2_cs.y);

    // угол второго сустава
    auto j1 = atan2a(j2_cs.y, j2_cs.x);
    t("j1: %f ", j1);

    // угол третьего сустава
    auto cs3 = t2d - j2_cs;
    t("cs3: %f %f ", cs3.x, cs3.y);
    auto j2 = atan2a(cs3.y, cs3.x) - j1;
    t("j2: %f\n", j2);

    return { j0, j1, j2 };
  }

};

class Spider{
public:
  Leg legs[8];
  void sit(double y) {
    auto as = Leg::calcAngles({250, y, -250});
    for (auto i = 0; i < 8; i++) {
      legs[i].moveTo(as);
    }
  }

  void setCoords(Coords3d cs) {
    auto as = Leg::calcAngles(cs);
    for (auto i = 0; i < 8; i++) {
      legs[i].moveTo(as);
    }
  }

  void flush() {
    legs[0].joints[0].brd->update();
    legs[4].joints[0].brd->update();
  }
};