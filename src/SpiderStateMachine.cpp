#include "SpiderStateMachine.h"

Coords3d SpiderStateMachine::baseCoords = {225, 0, -350};
constexpr double dist = 100;
constexpr double height = 100;


// x, y, высота
// Цикл шага, закодированный как смещения относительно нуля 
// на координаты, получаенные из InitOffsets
const std::vector<Coords3d> cycleA = {{
    { 0, 0, 1},
    { 1, 1, 1}, 
    { 1, 1, 0}, 
    { 0, 0, 0},
    {-1,-1, 0},
    {-1,-1, 1}, 
}};

const std::vector<Coords3d> cycleB = {{
    { 0, 0, 0},
    {-1,-1, 0},
    {-1,-1, 1},
    { 0, 0, 1}, 
    { 1, 1, 1}, 
    { 1, 1, 0}, 
}};


// Вычисляем координаты для ног 0 и 1. Координаты для всех остальных ног 
// мы получаем, поворачивая эти координаты на 90 градусов
//     ^
//     | -- angle == 0
//    __
//   /  \ 1
//  |    | 0 
//   \__/
static std::array<Coords3d, 2> InitOffsets(double angle, double dist, double height) {
    return {{
        {sin(angle) * dist, cos(angle) * dist, height},
        {sin(angle + PI / 4) * dist, cos(angle + PI / 4) * dist, height}
    }};
}


bool SpiderStateMachine::move(std::array<double, 2> input) {
    if(xSemaphoreTake(lock_input, 100 / portTICK_PERIOD_MS ) == pdTRUE ) {
        this->input.cmd = this->input.Move;
        this->input.data.move_vector = input;
        xSemaphoreGive(lock_input);
        return true;
    }
    return false;
}

bool SpiderStateMachine::turn(double input) {
    if(xSemaphoreTake(lock_input, 100 / portTICK_PERIOD_MS ) == pdTRUE ) {
        this->input.cmd = this->input.Turn;
        this->input.data.turn_speed = input;
        xSemaphoreGive(lock_input);
        return true;
    }
    return false;
}

bool SpiderStateMachine::sit(double input) {
    if(xSemaphoreTake(lock_input, 100 / portTICK_PERIOD_MS ) == pdTRUE ) {
        this->input.cmd = this->input.Sit;
        this->input.data.sit_height = input;
        xSemaphoreGive(lock_input);
        return true;
    }
    return false;
}

bool SpiderStateMachine::stop() {
    if(xSemaphoreTake(lock_input, 100 / portTICK_PERIOD_MS ) == pdTRUE ) {
        this->input.cmd = this->input.Stop;
        xSemaphoreGive(lock_input);
        return true;
    }
    return false;
}


void SpiderStateMachine::rtos_task(void * params) {
    auto m = static_cast<SpiderStateMachine*>(params);
    while (true) {
        Input_t input; 
        if (xSemaphoreTake(m->lock_input, portMAX_DELAY ) == pdTRUE ) {
            input = m->input;
            xSemaphoreGive(m->lock_input);

            switch (input.cmd) {
            case Input_t::Move:
            {
                auto offsets = InitOffsets(input.data.move_vector[0], input.data.move_vector[1] * dist, height);
                for (auto c = 0; c < cycleA.size(); c++) {
                    for (auto ll = 0; ll < 8; ll += 2) {
                        m->spider.legs[ll].moveTo(Leg::calcAngles(baseCoords + offsets[0] * cycleA[c]));
                        m->spider.legs[ll + 1].moveTo(Leg::calcAngles(baseCoords + offsets[1] * cycleB[c]));
                        offsets[0] = offsets[0].rot_y_90();
                        offsets[1] = offsets[1].rot_y_90();
                    }
                    m->spider.flush();
                    vTaskDelay(150 / portTICK_PERIOD_MS);
                }
                break;
            }
            case Input_t::Turn:
            {
                auto offsets = InitOffsets(PI, input.data.turn_speed * dist, height);
                for (auto c = 0; c < cycleA.size(); c++) {
                    for (auto ll = 0; ll < 8; ll += 2) {
                        m->spider.legs[ll].moveTo(Leg::calcAngles(baseCoords + offsets[0] * cycleA[c]));
                        m->spider.legs[ll + 1].moveTo(Leg::calcAngles(baseCoords + offsets[0] * cycleB[c]));
                    }
                    m->spider.flush();
                    vTaskDelay(150 / portTICK_PERIOD_MS);
                }
                break;
            }
            case Input_t::Sit:
            {
                for (auto l = 0; l < 8; l++) {
                    m->spider.legs[l].moveTo(Leg::calcAngles(baseCoords + Coords3d{0, 0, input.data.sit_height}));
                }
                m->spider.flush();
                vTaskDelay(150 / portTICK_PERIOD_MS);
                break;
            }
            case Input_t::Stop:
            {
                for (auto l = 0; l < 8; l++) {
                    m->spider.legs[l].moveTo(Leg::calcAngles(baseCoords));
                }
                m->spider.flush();
                vTaskDelay(150 / portTICK_PERIOD_MS);
                break;
            }
            default:
            vTaskDelay(150 / portTICK_PERIOD_MS);
                break;
            }
        }
    }
}