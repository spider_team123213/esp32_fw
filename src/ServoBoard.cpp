#include "ServoBoard.h"
#include <Wire.h>

ServoBoard::ServoBoard(uint8_t addr): addr(addr){
    for (size_t i = 0; i < 12; i++) {
        _pos[i] = pos[i] = 0;
    }
    //full_update();
};

void ServoBoard::full_update() {
    for (uint8_t i = 0; i < 12; i++) {
        Wire.beginTransmission(addr);
        Wire.write(0x10 | i);
        Wire.write(pos[i]);
        Wire.endTransmission();
        _pos[i] = pos[i];
    }
}

void ServoBoard::update() {
    for (uint8_t i = 0; i < 12; i++) {
        if (_pos[i] != pos[i]) {
            Wire.beginTransmission(addr);
            Wire.write(0x10 | i);
            Wire.write(pos[i]);
            Wire.endTransmission();
            _pos[i] = pos[i];
        }
    }
}