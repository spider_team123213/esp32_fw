#include "WebInterface.h"
#include "SpiderStateMachine.h"

extern SpiderStateMachine stateMachine;

void WebInterface::on_data(JsonDocument & doc) {
  const char* key = doc["key"];
  if (!key) {
    Serial.println("no key");
    return;
  }
  else if (strcmp(key, "pause") == 0) {
    Serial.printf("pausing %d\n", stateMachine.pause());
  } 
  else if (strcmp(key, "move") == 0) {
    stateMachine.move({doc["angle"].as<double>(), constrain(doc["speed"].as<double>(), 0, 1)});
    timeout.once_ms(800, reset, this);
  } 
  else if (strcmp(key, "turn") == 0) {
    stateMachine.turn(constrain(doc["speed"].as<double>(), -1, 1));
    timeout.once_ms(800, reset, this);
  } 
  else if (strcmp(key, "sit") == 0) {
    stateMachine.sit(constrain(doc["height"].as<double>(), 0, 1) * 250);
    timeout.detach();
  } 
  else {
    Serial.printf("unknown key %s\n", doc["key"].as<const char*>());
  }
}

void WebInterface::reset(WebInterface * wi) {
    stateMachine.stop();
}
