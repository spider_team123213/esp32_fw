#pragma once


class List {
    class ListItem {
        public:
        void* item;
        ListItem* next;
        ListItem(void* item): item(item), next(0) {}
    };

    List::ListItem* list;

    public:
    void add(void* item) {
        auto l = new ListItem(item);
        if (list) {
            l->next = list;
        }
        list = l;
    }

    void* item remove(void* item) {
        auto l = list;
        if (l->item == item) {
            list = l->next;
            delete l;
            return true;
        }
        while(l->next){
            auto p = l;
            l = l->next;
            if (l->item == item) {
                p->next = l->next;
                delete l;
                return true;
            }
        }
        return false;
    }
};


