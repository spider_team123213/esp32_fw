#include <Arduino.h>
#include <M5Stack.h>
#include <Wire.h>
#include <WiFi.h>
//#include <Hash.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <SPIFFS.h>
#include "websocket.h"
#include "ServoBoard.h"
#include <ArduinoJson.h>
#include "SpiderInit.h"
#include "SpiderStateMachine.h"
#include "WebInterface.h"

// Питание 1,5 + 2,3 + 3,5

AsyncWebServer server(80);
AsyncWebSocket ws("/s");

auto & clientList = WSClientList::get();

ServoBoard servoBoard1(0x53);
ServoBoard servoBoard2(0x54);

SpiderStateMachine stateMachine(spider);

WebInterface wi;

void setup() {
  Serial.begin(115200);
  esp_log_level_set("*", ESP_LOG_WARN);
  M5.begin(true, false, true);
  M5.Lcd.setTextFont(4);
  
  Wire.begin(21, 22, 100000);
  init_spider();

  WiFi.mode(WIFI_STA);
  WiFi.begin("MikroTik-Snegiri", "12345678");
  WiFi.setAutoReconnect(true);
  WiFi.onEvent([](WiFiEvent_t event, WiFiEventInfo_t info){
    M5.Lcd.print(WiFi.localIP());
  }, WiFiEvent_t::SYSTEM_EVENT_STA_GOT_IP);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    M5.Lcd.print("WiFi Failed!");
    return;
  }

  SPIFFS.begin(true);

  server.rewrite("/", "index.html");
  server.serveStatic("/", SPIFFS, "/www/");
  clientList.on_data = [](JsonDocument & doc) -> void {wi.on_data(doc);};
  clientList.bind(ws);
  server.addHandler(&ws);
  server.begin();

  stateMachine.init();
  M5.Speaker.beep();
}

void loop() {
  M5.update();

  if(M5.BtnA.wasReleased()) {
    stateMachine.setPwr(false);
  }
  if(M5.BtnC.wasReleased()) {
    stateMachine.setPwr(true);
  }

  M5.Lcd.setCursor(0, 45);
  M5.Lcd.print(esp_get_free_heap_size());

  delay(10);
}