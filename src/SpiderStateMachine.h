#pragma once
#include <Arduino.h>
#include "geometry.h"
#include "SpiderStructure.h"
#include "websocket.h"

static constexpr uint8_t SERVO_PWR = G5;

class SpiderStateMachine {
    public:
    SpiderStateMachine(
        Spider & s
    ) : spider(s) {};

    Spider & spider;
    static constexpr const char* tag = "SpiderStateMachine";
    struct Input_t {
        enum {
            Pause,
            Resume,
            Move,
            Turn,
            Sit,
            Stop
        } cmd;
        union {
            std::array<double, 2> move_vector; // Полярные координаты: угол, расстояние
            double turn_speed; // Угловая скорость
            double sit_height;
        } data;
    } input = { Input_t::Stop };

    bool setPwr(bool val) {
        WSClientList::get().set_val("pwr", static_cast<double>(val));
        digitalWrite(SERVO_PWR, !val);
        return true;
    }

    static Coords3d baseCoords;

    TaskHandle_t task_handle;
    SemaphoreHandle_t lock_input = xSemaphoreCreateMutex();

    xQueueHandle messageQueue = xQueueCreate(4, sizeof(Input_t));

    void init() {
        configASSERT(messageQueue);
        xTaskCreate(SpiderStateMachine::rtos_task, "SpiderStateMachine", 2048, this, 1, &task_handle);
        configASSERT(task_handle);
        pinMode(SERVO_PWR, OUTPUT);
        esp_log_level_set(tag, ESP_LOG_INFO);
        esp_log_write(ESP_LOG_INFO, tag, "statemachine init success");
        setPwr(true);
    }

    bool pause() {
        Input_t i = {Input_t::Pause};
        return pdTRUE == xQueueSend(messageQueue, &i, 0);
    }

    bool run() {
        Input_t i = {Input_t::Resume};
        return pdTRUE == xQueueSend(messageQueue, &i, 0);
    }

    bool stop();
    bool move(std::array<double, 2> input);
    bool turn(double input);
    bool sit(double input);
    static void rtos_task(void * params);
};


