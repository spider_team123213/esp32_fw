#include "websocket.h"
#include <ArduinoJson.h>
using namespace std::placeholders;
WSClientList::WSClientList(){}

WSClientList & WSClientList::get() {
  static WSClientList instance;
  return instance;
}

void WSClientList::set_val(std::string key, double value) {
  std::unique_ptr<Val> p(new NumberVal(value));
  vals[key] = std::move(p);
}

void WSClientList::set_val(std::string key, std::string string) {
  std::unique_ptr<Val> p(new StringVal(string));
  vals[key] = std::move(p);
}

void WSClientList::add_client(AsyncWebSocketClient * client) {
  clients.emplace(std::piecewise_construct, std::make_tuple(client->id()), std::make_tuple(client, this));
  char buf[256];
  for(auto & v: vals) {
    StaticJsonDocument<256> doc;
    auto obj = doc.to<JsonObject>();
    v.second->publish(obj, v.first);
    auto sz = serializeJson(doc, buf, 256);
    client->text(buf, sz);
  }
}

void WSClientList::remove_client(AsyncWebSocketClient * client) {
  clients.erase(client->id());
}

void WSClientList::wsEvent(AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t *data, size_t len) {
  if(type == WS_EVT_CONNECT){
    add_client(client);
    if (on_connect) on_connect(client);
  } else if(type == WS_EVT_DISCONNECT){
    remove_client(client);
  } else if(type == WS_EVT_ERROR){
    remove_client(client);
  } else if(type == WS_EVT_DATA){
    AwsFrameInfo * info = (AwsFrameInfo*)arg;
    buf_data(client, info, data, len);
  } 
}
void WSClientList::buf_data(AsyncWebSocketClient * client, AwsFrameInfo * info, uint8_t *data, size_t len) {
  clients.find(client->id())->second.buf_data(info, data, len);
}

void WSClientList::bind(AsyncWebSocket & ws) {
  ws.onEvent(std::bind(&WSClientList::wsEvent, this, _1, _2, _3, _4, _5, _6));
}

void WSClient::buf_data(AwsFrameInfo * info, uint8_t *data, size_t len) {
  StaticJsonDocument<255> doc;

  DeserializationError error = deserializeJson(doc, data, len);

  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return;
  }
  
  if (list->on_data) {
    list->on_data(doc);
  }
}

WSClient::WSClient(AsyncWebSocketClient * client, WSClientList * list): client(client), list(list) {
  //Serial.printf("live %d\n", client->id());
}

WSClient::~WSClient() {
    //Serial.printf("dead %d\n", client->id());
  client->close();
}