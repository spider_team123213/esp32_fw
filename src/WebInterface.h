#pragma once
#include <ArduinoJson.h>
#include <Ticker.h>

class WebInterface
{
    Ticker timeout;
public:
    void on_data(JsonDocument & doc);
    static void reset(WebInterface * wi);
};
