#pragma once
#include <Arduino.h>
class ServoBoard {
    uint8_t addr;
    uint16_t _pos[12];
    public:
    uint16_t pos[12];
    ServoBoard(uint8_t addr = 0x53);
    void full_update();
    void update();
};